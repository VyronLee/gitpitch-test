<!-- 
$theme: default
$size: 4:3
page_number: true 
-->


 客户端部分模块介绍
 =============
 
 ### 1. 实体模块
 ### 2. 地图编辑器（2D） 
 
---
 
 一、实体模块
 =========
 
 ##### 游戏所有实体以及相关控制逻辑 （EntityManager）
 
---
 
 一、实体模块
 =========
 
 ## 模块结构
 
 - Entity
 - Component
 - Define
 - Context

---

一、实体模块
=========

## 1.  Entity

- User（帐号）
- Space（场景）
- Actor （角色）
- FixObject （静态物件）
- Monster （怪物）
- Pet （宠物）

---

一、实体模块
=========

## 2. Component

- Property
- Action
- State
- View
- Network
- ...

---

一、实体模块
=========

## 2. Component

### 2-1. Property

- _实体属性存储， 分为Int与String_
- _属性加密（异或）_
- _Get/SetIntProperty, Get/SetStrProperty_

---

一、实体模块
=========

## 2. Component
    
### 2-2. Action
+ __Command__（操作指令）
	* ActionContextBase, 根据actionId进行服务器接口调用
+ __Handler__（操作指令处理）
	* ActionContextBase, 根据actionId进行分发处理

<br/>
Action类型：Idle, Run, Jump, Cast, Life<br/>

<br/>
问题：同步逻辑处理？<br/>
 
---


一、实体模块
=========

## 2. Component

### 2-2. Action
+ __AutoAction__（自动指令/连续指令）
	* Default（默认指令，待机，指令间连接）
	* Run（行走指令，沿着输入路线移动）
	* Jump（跳跃指令，根据输入启动/终点以及速度跳跃）
	* Fight（连续攻击指令，对目标不停普攻）
	* Life（生活技能指令）
	
<br/>
接口封装：AutoMove， AutoAttack， AutoUseLifeSkill， ...

---

一、实体模块
=========

## 2. Component

### 2-3. State （实体状态控制）
+ StateIdling
+ StateRunning
+ StateJumping
+ StateCasting
+ StateHurting
+ StateDeath

<br/>
1. 根据输入现场进行相关状态动画表现<br/>
2. 根据状态关系判决状态能否切换(EventDispatcher)<br/>

---

一、实体模块
=========

## 2. Component

### 2-3. View （实体视图）
+ 显示层，操作GameObject
+ 物理相关变量，如Position，Rotation，Velocity，Force
+ 模型组件切换（换装，座骑）

---

一、实体模块
=========

## 2. Component

### 2-4. NetworkTransport（基于实体的网络通信，RPC）

+ Base，RPC与OnRPC
+ Network+User
+ Network+Space
+ Network+Creature
+ Network+Actor
+ Network+Monster
+ ...

<br/>
问题：反射性能，Json解析性能？

---

一、实体模块
=========

## 3. Define

+ EntityDef
	- EntityType
	- PropType
	- StateDef
	- ActionDef
	- ...
+ EventDef

---

一、实体模块
=========

## 4. Context

+ EntityContext..
+ ActionContext..
+ AutoActionContext..
+ StateContext..
+ ViewContext..
+ RpcArgs..
+ ...

---

二、地图编辑器（2D）
==============

#### 生成地图分层结构， 设置错层表现

#### 刷格子， 设置跳跃点与传送点， 导出信息给客户端与服务器使用

